export const ROUTES = [
  '/',
  '/lazy',
  '/lazy/nested',
  '/polls',
  '/poll',
];
