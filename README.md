Run the app

1. RUN `npm install` to install the node module package
2. RUN `npm run build:ssr && npm run serve:ssr` to build and run the Server side application
3. Browse http://localhost:4000

Poll
1. the data publishDate require to x 1000 to fit Moment unix timestamp, in order to keep the Poll publish date more
flexible in milliseconds, better to do the data massage in Data access layer
2. Chart
    a. limitation: spent sometime try to display the value and % in chart, but not complete
    b. research: chart.piecelabel.js should help, but still not integrate
    
Expectation
1. When user vote the Poll, the chart will update immediately, no need to refresh, the data will save to async
2. Depends on requirement, but normally everyone should vote one on each poll, front-end could add some checking thus
   disable the button after vote, also get this from back-end when init
3. If poll allow to change voted option, we need one more end-point to know the current user choice, and vote up the
   new option and vote down the other choice  
4. I apologize I can't complete all the task in time, remaining task should
    a. hookup the api end-point to get the data and fetch into the component
    b. Implement Today poll component, reuse the poll component to display the poll and custom the display, including
        the publish date and the background
    c. Implement the questions (poll-list) component to display the question list