import * as fs from 'fs';
import * as path from 'path';

export class PollController {
  /**
   * List the Poll questions
   * 
   * @param req
   * @param res
   * @param next
   */
  static list(req, res, next) {
    res.json(JSON.parse(fs.readFileSync(path.resolve('./dist/server/assets/poll.json'), 'utf8')));
  }

  static result(req, res, next) {
    res.json(JSON.parse(fs.readFileSync(path.resolve('./dist/server/assets/result.json'), 'utf8')));
  }
}