import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TodayPollComponent } from './today-poll/today-poll.component';
import { PollModule } from './poll/poll.module';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        TodayPollComponent
      ],
      imports: [ RouterTestingModule, PollModule ]
    }).compileComponents();
  }));

  it('should return true when call appComponent.test()', async(() => {
    const app = new AppComponent();
    expect(app.test()).toEqual(true);
  }));

});
