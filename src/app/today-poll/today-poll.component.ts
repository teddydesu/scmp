import { Component } from '@angular/core'

interface Option {
  id: number;
  label: string;
  count: number;
}

@Component({
  selector: 'today-poll',
  template: `
    <div class="container">
      <div class="today-poll-container">Today's Poll</div>
      <div class="poll-container">
        <poll [title]="title" [options]="options"></poll>
      </div>
    </div>`,
  styleUrls: [
    './today-poll.css'
  ]
})
export class TodayPollComponent {
  options: Option[] = [
    { id: 1, label: 'YES', count: 2 },
    { id: 2, label: 'NO', count: 4 },
  ];

  title: string = 'Is bitcoin worth the time and money that mining requires?';
  publishedDate: number = 1516605447;
}
