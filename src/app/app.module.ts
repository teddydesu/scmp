import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TodayPollComponent } from './today-poll/today-poll.component';
import { PollListComponent } from './poll-list/poll-list.component';
import { PollModule } from './poll/poll.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TodayPollComponent,
    PollListComponent
  ],
  imports: [
    PollModule,
    NgbModule.forRoot(),
    BrowserModule.withServerTransition({appId: 'my-app'}),
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full'},
      { path: 'lazy', loadChildren: './lazy/lazy.module#LazyModule'},
      { path: 'lazy/nested', loadChildren: './lazy/lazy.module#LazyModule'},
      { path: 'poll', loadChildren: './poll/poll.module#PollModule'}
    ])
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
