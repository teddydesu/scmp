import { TestBed, async } from '@angular/core/testing';
import { PollComponent } from './poll.component';
import { RouterTestingModule } from '@angular/router/testing';
import * as _ from 'lodash';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PollComponent,
      ],
      imports: [ RouterTestingModule ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();
  }));

  it('should contain vote method', async(() => {
    const id = 1;
    const poll = new PollComponent();
    spyOn(poll, 'vote');
    poll.vote(id);
    expect(poll.vote).toHaveBeenCalled();
  }));

  it('should return the total count', async(() => {
    const poll = new PollComponent();
    poll.options = [
      { id: 1, label: '123', count: 2 },
      { id: 2, label: '123456', count: 4 },
    ];
    expect(poll.calculateTotalVote()).toEqual(6);
  }));

  it('should format the publish date', async(() => {
    const poll = new PollComponent();
    poll.publishedDate = 1516605447 * 1000;
    expect(poll.getPublishDate()).toEqual('Monday, January 22, 2018 3:17 PM');
  }));

  it('should increase the vote count', async(() => {
    const poll = new PollComponent();
    // ugly mock - the angular2-chartjs lib is hard to test 
    poll.chartComponent = {
      chart: jasmine.createSpyObj('chart', ['update'])
    };
    poll.options = [
      { id: 1, label: '123', count: 2 },
      { id: 2, label: '123456', count: 4 },
    ];
    poll.data = {
      datasets: [
        {
          data: [1,2],
        }
      ]
    };
    poll.vote(1);
    expect(poll.options[0].count).toEqual(3);
  }));
});
