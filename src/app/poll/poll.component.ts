import { Component, Input, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import * as Moment from 'moment';
import { ChartComponent } from 'angular2-chartjs';

interface Option {
  id: number;
  label: string;
  count: number;
}

@Component({
  selector: 'poll',
  template: `
    <div class="container title title-container">
      <div class="row d-none d-lg-block">{{title}}</div>
    </div>

    <div class="container published-container">
      <div class="row d-none d-lg-block text-right">
        PUBLISHED: {{_published}}
      </div>
    </div>

    <div class="container poll">
      <div class="row align-self-sm-center d-lg-none mobile-title">{{title}}</div>
      <div class="row">
        <div class="col-sm-6">
          <ul class="options-list">
            <li *ngFor="let answer of options">
              <button class="btn btn-secondary btn-no-border-radius" (click)="vote(answer.id)">{{answer.label}}</button>
            </li>
          </ul>
        </div>
        <div class="col-sm-6">
          <chart [type]="type" [data]="data" [options]="chartOptions"></chart>
        </div>
      </div>

      <div class="row total-number-container">
        Total number of votes recorded: {{totalVotes}}
      </div>
    </div>
  `,
  styleUrls: [
    './poll.css'
  ]
})
export class PollComponent {
  @ViewChild(ChartComponent) chartComponent: any;

  @Input() title: string;
  @Input() publishedDate: number;
  @Input() options: Option[];
  totalVotes: number;
  _published: string;

  type: string = 'pie';
  data: any;

  chartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    rotation: 0.5 * Math.PI,
    legend: false
  };

  ngOnInit() {
    this._published = this.getPublishDate();
    this.reset();
  }

  private reset() {
    this.totalVotes = this.calculateTotalVote();

    // TODO: extract the chart to another component
    // limitation: spent sometime try to display the value and % in chart, but not complete
    // research: chart.piecelabel.js should help, but still not integrate
    this.data = {
      labels: _.map(this.options, 'label'),
      datasets: [
        {
          pieceLabel: {
            render: 'value'
          },
          data: _.map(this.options, 'count'),
          backgroundColor: [
            '#cc774a',
            '#243d66'
          ]
        },
        {
          labels: ['%'],
          pieceLabel: {
            render: 'label'
          },
          data: [100],
          backgroundColor: 'transparent',
          borderWidth: 0
        }]
    };
  }

  getPublishDate() {
    return Moment(this.publishedDate).format('LLLL');
  }

  calculateTotalVote() {
    return _.sumBy(this.options, 'count');
  }

  vote(id) {
    _.map(this.options, (option) => {
      if (option.id === id) {
        option.count++;
      }
    });

    // this.data.datasets[0].data = _.map(this.options, 'count');
    this.reset();
    this.chartComponent.chart.update();
  }
}
