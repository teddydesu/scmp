import { NgModule, Component } from '@angular/core'
import {RouterModule} from '@angular/router'
import { PollComponent } from './poll.component';
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'angular2-chartjs';

interface Option {
  id: number;
  label: string;
  count: number;
}

@Component({
  selector: 'poll-view',
  template: `<poll [title]="title" [options]="options"></poll>`
})

export class PollViewComponent {
  options: Option[] = [
    { id: 1, label: 'YES', count: 2 },
    { id: 2, label: 'NO', count: 4 },
  ];

  title: string = 'Is bitcoin worth the time and money that mining requires?';
  publishedDate: number = 1516605447;
}

@NgModule({
  declarations: [
    PollComponent,
    PollViewComponent
  ],
  imports: [
    BrowserModule,
    ChartModule,
    RouterModule.forChild([
      { path: '', component: PollViewComponent, pathMatch: 'full'}
    ])
  ],
  exports: [
    PollComponent,
    PollViewComponent
  ]
})
export class PollModule {

}