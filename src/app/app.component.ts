import { Component } from '@angular/core';
import { PollDao } from './dao/poll-dao';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  template: `
  <!--<h1>Universal Demo using Angular and Angular CLI</h1>-->
  <!--<a routerLink="/">Home</a>-->
  <!--<a routerLink="/lazy">Lazy</a>-->
  <!--<a routerLink="/lazy/nested">Lazy_Nested</a>-->
  <!--<a routerLink="/poll-list">Poll list</a>-->
  <!--<a routerLink="/poll">Poll</a>-->
  <!--<a routerLink="/today-poll">Today poll</a>-->
  <!--<router-outlet></router-outlet>-->
  <!--<poll-view [options]="poll.options" [publishedDate]="poll.publishedDate" [title]="poll.title"></poll-view>-->
  <poll-view></poll-view>`,
  styles: [
  ]
})
export class AppComponent {
  poll: any;
  
  public test() {
    return true;
  }

  ngOnInit() {
    PollDao
      .getPoll()
      .then(data => {
        console.log(data);
        const _poll = data.polls.pop();
        
        // TODO: request result data and append to option.count
        _.map(_poll, (poll: any) => {
          _.map(poll.answer.options, (option: any) => {
            option.count = Math.random() * 100;
          });
        });
        this.poll = _poll;
      })
  }
}
