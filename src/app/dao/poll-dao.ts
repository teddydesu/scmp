import * as Request from 'request-promise';

export class PollDao {
  static getPoll() {
    return Request({
      uri: 'http://localhost:4000/api/polls',
      json: true
    });
  }
}